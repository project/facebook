=== Functionality ===

This module will allow users to authenticate against Facebook.  It is rather buggy, but the basics work some of the time.

* /facebook - shows the user's Facebook name and friends
* /facebook/user/* - shows name of a given user
* /facebook/login - authenticates a user against Facebook and creates a local Drupal account

=== Future development ===

The functionality is basic and buggy.  It is hoped that users will file bug reports for this module.

=== Requirements ===

* A developer account at Facebook
  - <http://developers.facebook.com/account.php>
  - Generate an API Key and Secret for entering into /admin/settings/facebook
  - Set Callback URL to <http://example.com/facebook>

=== Credits ===

* Ryan Amos <support@humorsoftware.com> - Facebook PHP4 Library
* CAS.module - Inspiration for design and some of the user authentication code
