<?php

/**
* Author: Ryan Amos
* Credits: Based off of Facebook's PHP5 client.
* Version: 1.0
*/
class Facebook
{
	var $secret;
	var $session_key;
	var $api_key;
	var $server_addr;
	var $desktop;
	var $session_secret;
	var $throw_errors;
	var $last_call_success;
	var $last_error;
	var $debug;

	function Facebook ($api_key, $secret, $session_key=null, $desktop=false, $throw_errors=true, $debug=false)
	{
		$this->secret = $secret;
		$this->session_key = $session_key;
		$this->api_key = $api_key;
		$this->desktop = $desktop;
		$this->debug = $debug;
		$this->throw_errors = $throw_errors;
		$this->last_call_success = true;
		$this->last_error = null;
		$this->server_addr = 'http://api.facebook.com/restserver.php';
	}

	/**
	* Retrieves the events of the currently logged in user between the provided
	* UTC times
	* @param int $start_time UTC lower bound
	* @param int $end_time UTC upper_bound
	* @return array of friends
	*/
	function events_getInWindow($start_time, $end_time)
	{
		return $this->call_method('facebook.events.getInWindow', array('start_time' => $start_time, 'end_time' => $end_time));
	}

	/**
	* Retrieves the friends of the currently logged in user.
	* @return array of friends
	*/
	function friends_get()
	{
		return $this->call_method('facebook.friends.get', array());
	}

	/**
	* Retrieves the friends of the currently logged in user,filtered by socialmap type
	* @param string link_type type denoted by string, e.g. "Coworkers"
	* @return array of friends
	*/
	function friends_getTyped($link_type)
	{
		return $this->call_method('facebook.friends.getTyped', array('link_type' => $link_type));
	}

	/**
	* Retrieves the requested info fields for the requested set of user.
	* @param array $users_arr an array of user ids to get info for
	* @param array $field_array an array of strings describing the info fields desired
	* @return an array of arrays of info fields, which may themselves be arrays :)
	*/
	function users_getInfo($users_arr, $field_array)
	{
		return $this->call_method('facebook.users.getInfo', array('users' => $users_arr, 'fields' => $field_array));
	}

	/**
	* Retrieves the photos that have been tagged as having the given user.
	* @param string $id  the id of the user in the photos.
	* @param int    $max the number of photos to get (if 0 returns all of them)
	* @returns an array of photo objects.
	*/
	function photos_getOfUser($id, $max=0)
	{
		return $this->call_method('facebook.photos.getOfUser', array('id'=>$id, 'max'=>$max));
	}

	/**
	* Retrieves the albums created by the given user.
	* @param string $id the id of the user whose albums you want.
	* @returns an array of album objects.
	*/
	function photos_getAlbums($id)
	{
		return $this->call_method('facebook.photos.getAlbums', array('id'=>$id));
	}

	/**
	* Retrieves the photos in a given album.
	* @param string $aid the id of the album you want, as returned by photos_getAlbums.
	* @param string $uid the id of the user whose albums you want.
	* @returns an array of photo objects.
	*/
	function photos_getFromAlbum($aid, $uid)
	{
		return $this->call_method('facebook.photos.getFromAlbum', array('aid'=>$aid, 'id'=>$uid));
	}

	/**
	* Retrieves the counts of unread and total messages for the current user.
	* @return an associative array with keys 'unread' and 'total'
	*/
	function messages_getCount()
	{
		return $this->call_method('facebook.messages.getCount', array());
	}

	/**
	* Retrieves the number of wall posts for the specified user.
	* @param string $id the id of the user to get the wall count for (leave 
	*                   blank for the count of the current user).
	* @return an int representing the number of posts
	*/
	function wall_getCount($id='')
	{
		return $this->call_method('facebook.wall.getCount', array('id'=>$id));
	}

	/**
	* Retrieves the number of comments on the current user's photos.
	* @return an int representing the number of comments
	*/
	function photos_getCommentCount()
	{
		return $this->call_method('facebook.photos.getCommentCount', array());
	}

	/**
	* Retrieves the number of pokes (unseen and total) for the current user.
	* @return an associative array with keys 'unseen' and 'total'
	*/
	function pokes_getCount()
	{
		return $this->call_method('facebook.pokes.getCount', array());
	}

	/**
	* Retrieves whether or not two users are friends (note: this is reflexive, 
	* the params can be swapped with no effect).
	* @param array $id1: array of ids of some length X
	* @param array $id2: array of ids of SAME length X
	* @return array of elements in {0,1} of length X, indicating whether each pair are friends
	*/
	function friends_areFriends($id1, $id2)
	{
		return $this->call_method('facebook.friends.areFriends', array('id1'=>$id1, 'id2'=>$id2));
	}

	/**
	* Intended for use by desktop clients.  Call this function and store the
	* result, using it first to generate the appropriate login url and then to
	* retrieve the session information.
	* @return assoc array with 'token' => the auth_token string to be passed into login.php and auth_getSession.
	*/
	function auth_createToken()
	{
		return $this->call_method('facebook.auth.createToken', array());
	}

	/**
	* Call this function to retrieve the session information after your user has 
	* logged in.
	* @param string $auth_token the token returned by auth_createToken or passed back to your callback_url.
	*/
	function auth_getSession($auth_token)
	{
		if ($this->desktop)
		{
			// temporarily swap to using https for the sake of getting the session secret
			$real_server_addr = $this->server_addr;
			$this->server_addr = str_replace('http://', 'https://', $real_server_addr);
		}

		$result = $this->call_method('facebook.auth.getSession', array('auth_token'=>$auth_token));
		$this->session_key = $result['session_key'];

		if ($this->desktop)
		{
			$this->session_secret = $result['secret'];
			$this->server_addr = $real_server_addr;
		}
		return $result;
	}

	/**
	* Call this function to post methods to the rest server.
	* @param string $method: string of action to complete.
	* @param array $params: array of parameters to pass to rest server.
	* @return array of elements parsed from xml which was returned by rest server.
	*/
	function call_method($method, $params)
	{
		$this->last_call_success = true;
		$this->last_error = null;

		$xml = $this->post_request($method, $params);
		$result = $this->xml2array($xml);

		if ($this->debug)
		{
			// output the raw xml and its corresponding php object, for debugging:
			print '<div style="width: 100%; clear: both;">';
			print '<div style="float: left; overflow: auto; width: 350px;"><pre>'.htmlspecialchars($xml).'</pre></div>';
			print '<div style="float: left; overflow: auto; width: 350px;"><pre>'.print_r($result, true).'</pre></div>';
			print '</div>';
		}

		if (is_array($result) && ($result[0]['elements'][0]['name'] == 'fb_error'))
		{
			$this->last_call_success = false;
			$this->last_error = $result[0]['elements'][0]['elements'][1]['text'];
		}

		return $result;
	}

	/**
	* Call this function to post methods to the rest server.
	* @param string $method: string of action to complete.
	* @param array $params: array of parameters to pass to rest server.
	* @return string of unparsed xml data returned by rest server.
	*/
	function post_request($method, $params)
	{
		$params['method'] = $method;
		$params['session_key'] = $this->session_key;
		$params['api_key'] = $this->api_key;
		$params['call_id'] = $this->microtime_float();

		$post_params = array();
		foreach ($params as $key => $val)
		{
			if (is_array($val))
			{
				$val = implode(',', $val);
			}
			$post_params[] = $key.'='.urlencode($val);
		}

		if ($this->desktop && $method != 'facebook.auth.getSession' && $method != 'facebook.auth.createToken')
		{
			$secret = $this->session_secret;
		}
		else
		{
			$secret = $this->secret;
		}

		$post_params[] = 'sig='.$this->api_generate_sig($params, $secret);
		$post_string = implode('&', $post_params);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->server_addr);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);

		return $result;
	}

	/**
	* Generate a signature for the API call.  Should be copied into the client
	* library and also used on the server to validate signatures.
	*
	* @author ari
	*/
	function api_generate_sig($params_array, $secret)
	{
		$str = '';

		ksort($params_array);
		foreach ($params_array as $k=>$v)
		{
			if ($k != 'sig')
			$str .= "$k=$v";
		}
		$str .= $secret;

		return md5($str);
	}

	/** 
	* Function to emulate the microtime() function in php5.
	*/
	function microtime_float()
	{
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}

	/**
	* Function to return xml data into an array.
	*/
	function xml2array ($xml)
	{
		$xmlary = array();
        
		$ReElements = '/<(\w+)\s*([^\/>]*)\s*(?:\/>|>(.*)<\/\s*\\1\s*>)/Us';
		$ReAttributes = '/(\w+)=(?:"|\')([^"\']*)(:?"|\')/';
        
		preg_match_all($ReElements, $xml, $elements);
		foreach ($elements[1] as $ie => $xx)
		{
			$xmlary[$ie]['name'] = $elements[1][$ie];
			if ( $attributes = trim($elements[2][$ie]))
			{
				preg_match_all($ReAttributes, $attributes, $att);
				foreach ($att[1] as $ia => $xx)
				{
					$xmlary[$ie]['attributes'][$att[1][$ia]] = $att[2][$ia];
				}
			}
            
			$cdend = strpos($elements[3][$ie], '<');
			if ($cdend > 0)
			{
				$xmlary[$ie]['text'] = substr($elements[3][$ie], 0, $cdend -1);
			}

			if (preg_match($ReElements, $elements[3][$ie]))       
			{
				$xmlary[$ie]['elements'] = $this->xml2array($elements[3][$ie]);
			}
			else if ($elements[3][$ie])
			{
				$xmlary[$ie]['text'] = $elements[3][$ie];
			}
		}
        
		return $xmlary;
	}
}
?>
